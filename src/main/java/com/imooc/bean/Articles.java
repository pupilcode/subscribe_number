package com.imooc.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 图文消息
 *
 * @author WangYong
 * @create 2018-05-29 10:52
 */
@Setter
@Getter
@ToString
public class Articles {
    private String Title;
    private String Description;
    private String PicUrl;
    private String Url;
}
