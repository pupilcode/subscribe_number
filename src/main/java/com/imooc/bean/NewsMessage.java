package com.imooc.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author WangYong
 * @create 2018-05-29 10:55
 */
@Setter
@Getter
@ToString
public class NewsMessage extends BaseMessage {
    private int ArticleCount;
    private List<com.imooc.bean.Articles> Articles;
}
