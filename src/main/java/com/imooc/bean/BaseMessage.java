package com.imooc.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 基础参数
 *
 * @author WangYong
 * @create 2018-05-29 10:56
 */
@Setter
@Getter
@ToString
public class BaseMessage {
    /**
     * 开发者微信号
     */
    private String ToUserName;
    /**
     * 发送方帐号（一个OpenID）
     */
    private String FromUserName;
    /**
     * 消息创建时间 （整型）
     */
    private Long CreateTime;
    /**
     * text
     */
    private String MsgType;
}
