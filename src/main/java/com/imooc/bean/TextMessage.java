package com.imooc.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 接收普通消息实体
 *
 * @author WangYong
 * @create 2018-05-22 17:54
 */

@Setter
@Getter
@ToString
public class TextMessage extends BaseMessage {

    /**
     * 文本消息内容
     */
    private String Content;
    /**
     * 消息id，64位整型
     */
    private Long MsgId;
}
