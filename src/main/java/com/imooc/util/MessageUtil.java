package com.imooc.util;

import com.imooc.bean.Articles;
import com.imooc.bean.NewsMessage;
import com.imooc.bean.TextMessage;

import java.util.ArrayList;
import java.util.List;

import static com.imooc.util.Convert.newsMessageToXML;

/**
 * @author WangYong
 * @create 2018-05-23 18:35
 */
public class MessageUtil {
    public static final String MESSAGE_TEXT = "text";
    public static final String MESSAGE_NEWS = "news";
    public static final String MESSAGE_IMAGE = "image";
    public static final String MESSAGE_VOICE = "voice";
    public static final String MESSAGE_VIDEO = "video";
    public static final String MESSAGE_LINK = "link";
    public static final String MESSAGE_LOCATION = "location";
    public static final String MESSAGE_EVENT = "event";
    public static final String MESSAGE_EVENT_SUBSCRIBE = "subscribe";
    public static final String MESSAGE_EVENT_UNSUBSCRIBE = "unsubscribe";
    public static final String MESSAGE_EVENT_CLICK = "CLICK";
    public static final String MESSAGE_EVENT_VIEW = "VIEW";
    // public static final String projectUrl = "http://52.15.120.148/dingyuehao/";
    public static final String projectUrl = "http://qing.viphk.ngrok.org/Weixin/";

    /**
     * 初始化用户发送的信息
     *
     * @param toUserName   发给谁
     * @param fromUserName 谁发的
     * @param content      返回内容
     * @return
     */
    public static TextMessage convertUserTextMessage(String toUserName, String fromUserName, String content) {
        TextMessage reqMessage = new TextMessage();
        //将map存储到bean中
        reqMessage.setToUserName(toUserName);
        reqMessage.setFromUserName(fromUserName);
        reqMessage.setMsgType(MessageUtil.MESSAGE_TEXT);
        reqMessage.setCreateTime(System.currentTimeMillis());
        reqMessage.setContent(content);
        reqMessage.setCreateTime(System.currentTimeMillis());
        return reqMessage;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * 组装向用户发送的文本消息
     *
     * @param toUserName
     * @param fromUserName
     * @param content
     * @return
     */
    public static String initTextMessage(String toUserName, String fromUserName, String content) {
        //将返回实体转换成XML
        TextMessage reqMessage = new TextMessage();
        //将map存储到bean中
        reqMessage.setToUserName(toUserName);
        reqMessage.setFromUserName(fromUserName);
        reqMessage.setMsgType(MessageUtil.MESSAGE_TEXT);
        reqMessage.setCreateTime(System.currentTimeMillis());
        reqMessage.setContent(content);
        reqMessage.setCreateTime(System.currentTimeMillis());
        String convertXml = Convert.textMessageToXml(reqMessage);
        return convertXml;
    }

    /**
     * 主菜单
     *
     * @return
     */
    public static String menuText() {
        StringBuffer sb = new StringBuffer();
        sb.append("欢迎您的关注,请按照菜单提示进行操作:\n\n");
        sb.append("1,课程介绍\n");
        sb.append("2,慕课网接口\n");
        sb.append("回复?调出此窗口\n");
        return sb.toString();
    }

    /**
     * 第一个菜单
     *
     * @return
     */
    public static String firstMenu() {
        StringBuffer sb = new StringBuffer();
        sb.append("本套课程介绍微信公众号开发,主要涉及公众号介绍,编辑模式介绍,开发模式介绍等");
        return sb.toString();
    }

    /**
     * 第二个菜单
     *
     * @return
     */
    public static String secondMenu() {
        StringBuffer sb = new StringBuffer();
        sb.append("慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。");
        return sb.toString();
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * 组装向用户发送的图文消息
     *
     * @param toUserName
     * @param fromUserName
     * @return
     */
    public static String  initNewsMessage(String toUserName, String fromUserName) {
        String mewssage = null;
        NewsMessage newsMessage = new NewsMessage();
        List<Articles> newsList = new ArrayList<Articles>();
        Articles articles = new Articles();
        articles.setTitle("慕课网介绍");
        articles.setDescription("balabala~~~~~~~~~~");
        articles.setPicUrl(projectUrl + "image/image1.jpg");
        articles.setUrl("www.baidu.com");
        newsList.add(articles);

        Articles articles2 = new Articles();
        articles2.setTitle("慕课网介绍2");
        articles2.setDescription("2balabala~~~~~~~~~~");
        articles2.setPicUrl(projectUrl + "image/image1.jpg");
        articles2.setUrl("www.baidu.com");
        newsList.add(articles2);

        Articles articles3 = new Articles();
        articles3.setTitle("慕课网介绍3");
        articles3.setDescription("2balabala~~~~~~~~~~");
        articles3.setPicUrl(projectUrl + "image/image1.jpg");
        articles3.setUrl("www.baidu.com");
        newsList.add(articles3);

        newsMessage.setFromUserName(toUserName);
        newsMessage.setToUserName(fromUserName);
        newsMessage.setCreateTime(System.currentTimeMillis());
        newsMessage.setMsgType(MESSAGE_NEWS);
        newsMessage.setArticles(newsList);
        newsMessage.setArticleCount(newsList.size());
        return newsMessageToXML(newsMessage);
    }
}
