package com.imooc.servlet;

import com.imooc.bean.TextMessage;
import com.imooc.util.CheckUtil;
import com.imooc.util.Convert;
import com.imooc.util.MessageUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author WangYong
 * @create 2018-05-22 16:19
 */
public class WeixinServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String signature = req.getParameter("signature");
        String timestamp = req.getParameter("timestamp");
        String nonce = req.getParameter("nonce");
        String echostr = req.getParameter("echostr");

        boolean chekckSignture = CheckUtil.chekckSignture(signature, timestamp, nonce);
        System.out.println("doget~~~~~");
        if (chekckSignture) {
            PrintWriter out = resp.getWriter();
            out.print(echostr);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        //防止中文乱码
        request.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        TextMessage reqMessage;
        PrintWriter out = resp.getWriter();
        try {
            //把微信请求的xml转换成map
            Map<String, String> map = Convert.parseXml(request);
            String msgType = map.get("MsgType");
            String fromUserName = map.get("FromUserName");
            String toUserName = map.get("ToUserName");
            String content = map.get("Content");
            //初始化接口Bean
            reqMessage = MessageUtil.convertUserTextMessage(toUserName, fromUserName, content);
            System.out.println("转换数据bean:" + reqMessage);

            // 返回的XML
            String returnXmlMessage = null;
            //判断信息类型
            if (MessageUtil.MESSAGE_TEXT.equals(msgType)) {
                if ("1".equals(content)) {
                    returnXmlMessage = MessageUtil.initTextMessage(reqMessage.getFromUserName(), reqMessage.getToUserName(), MessageUtil.firstMenu());
                } else if ("2".equals(content)) {
                    // returnXmlMessage = MessageUtil.initTextMessage(reqMessage.getFromUserName(), reqMessage.getToUserName(), MessageUtil.secondMenu());
                    returnXmlMessage = MessageUtil.initNewsMessage(reqMessage.getToUserName(),reqMessage.getFromUserName());
                } else if ("?".equals(content) || "？".equals(content)) {
                    returnXmlMessage = MessageUtil.initTextMessage(reqMessage.getFromUserName(), reqMessage.getToUserName(), MessageUtil.menuText());
                }
            } else if (MessageUtil.MESSAGE_EVENT.equals(msgType)){
                String eventType = map.get("Event");
                if (MessageUtil.MESSAGE_EVENT_SUBSCRIBE.equals(eventType)) {
                    MessageUtil.initTextMessage(reqMessage.getFromUserName(), reqMessage.getToUserName(), "您发送的消息是:" + reqMessage.getContent());
                }
            }
            System.out.println("" + returnXmlMessage);
            //返回xml
            out.print(returnXmlMessage);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
/**
 * 返回的时候,开发者的账号就成了发送方账号
 * 开发者微信号就成了openId
 * 就是一个发送方和接收方的关系(来自谁,发给谁)
 */